package com.hendisantika.paymentservice.service;

import com.hendisantika.paymentservice.entity.Payment;
import com.hendisantika.paymentservice.repository.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Random;
import java.util.UUID;

/**
 * Created by IntelliJ IDEA.
 * Project : payment-service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 26/05/21
 * Time: 20.58
 */
@Service
public class PaymentService {

    @Autowired
    private PaymentRepository paymentRepository;

    public Payment doPay(Payment payment) {
        payment.setPaymentStatus(paymentStatus());
        payment.setTxId(UUID.randomUUID().toString());
        return paymentRepository.save(payment);
    }

    private String paymentStatus() {
        //mocking a 3rd party payment call like GoPay,OVO, ShopeePay, etc.
        return new Random().nextBoolean() ? "success" : "failure";
    }

}
