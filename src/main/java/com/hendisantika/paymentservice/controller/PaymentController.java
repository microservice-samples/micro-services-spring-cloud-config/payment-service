package com.hendisantika.paymentservice.controller;

import com.hendisantika.paymentservice.entity.Payment;
import com.hendisantika.paymentservice.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by IntelliJ IDEA.
 * Project : payment-service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 26/05/21
 * Time: 21.00
 */
@RestController
@RequestMapping("/payment")
public class PaymentController {
    @Autowired
    public PaymentService paymentService;

    @PostMapping("/doPay")
    public Payment doPayment(@RequestBody Payment payment) {
        return paymentService.doPay(payment);
    }
}
