package com.hendisantika.paymentservice.repository;

import com.hendisantika.paymentservice.entity.Payment;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : payment-service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 26/05/21
 * Time: 20.57
 */
public interface PaymentRepository extends JpaRepository<Payment, Integer> {
}
